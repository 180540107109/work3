package com.aswdc.secondday;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class formActivity extends AppCompatActivity {

    EditText etFirstName, etLastName;
    /*ImageView ivClose;*/
    EditText etEmail;

    Button btnSubmit;
    /*ImageView ivBackground;*/
    RadioGroup rgGender;
    RadioButton rbMale;
    RadioButton rbFemale;

    CheckBox chbCricket;
    CheckBox chbFootBall;
    CheckBox chbHockey;
    EditText etPhoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        initViewReference();
        initViewEvent();
        //setTypefaceOnView();
        handleSavedInstance(savedInstanceState);

    }

    void handleSavedInstance(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            Log.d("BUNDLE VALUE:::", "" + savedInstanceState.toString());
            /*tvDisplay.setText(savedInstanceState.getString("TextViewValue"));*/
        }
    }

    void setTypefaceOnView() {
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/BalsamiqSans-Bold.ttf");
        btnSubmit.setTypeface(typeface);
        /*tvDisplay.setTypeface(typeface);*/
        etFirstName.setTypeface(typeface);
        etLastName.setTypeface(typeface);
        etEmail.setTypeface(typeface);
    }

    void initViewEvent() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if (isvalid()) {


                    // get the text to pass
                    String textToPass = etFirstName.getText().toString() + " " + etLastName.getText().toString();

                    // start the SecondActivity
                    Intent intent = new Intent(formActivity.this, DisplayActivity.class);
                    intent.putExtra("Name", textToPass);
                    intent.putExtra("PhoneNumber", etPhoneNumber.getText().toString());
                    startActivity(intent);

                    //Toast.makeText(getApplicationContext(), rbMale.isChecked() ? "Male" : "Female", Toast.LENGTH_LONG).show();
                }
            }

        });
        /*ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });*/
       /* tvDisplay.setOnClickListener(new View.onClickListeners()
        {
            @Override
            public void onClick(View view) {

            }
        });*/
       rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            if (i == R.id.rbActMale) {
                chbCricket.setVisibility(View.VISIBLE);
                chbFootBall.setVisibility(View.VISIBLE);
                chbHockey.setVisibility(View.VISIBLE);
            } else if (i == R.id.rbActFemale) {
                chbCricket.setVisibility(View.VISIBLE);
                chbFootBall.setVisibility(View.VISIBLE);
                chbHockey.setVisibility(View.GONE);
            }
        }
    });
    }

    void initViewReference() {
        etFirstName = findViewById(R.id.etActFirstName);
        etLastName = findViewById(R.id.etActLastName);
        /*ivClose = findViewById(R.id.ivActClose);*/
        btnSubmit = findViewById(R.id.btnActSubmit);
        rgGender = findViewById(R.id.rgActGender);
        chbCricket = findViewById(R.id.chbActCricket);
        chbFootBall = findViewById(R.id.chbActFootBall);
        chbHockey = findViewById(R.id.chbActHockey);
        etPhoneNumber = findViewById(R.id.etActPhoneNumber);
        etEmail=findViewById(R.id.etActemail);
        /*ivBackground = findViewById(R.id.ivActBackground);*/
    }

    //validation.
    boolean isvalid() {
        boolean flag = true;
        if (TextUtils.isEmpty(etFirstName.getText())) {
            etFirstName.setError(getString(R.string.enter_value));

            flag = false;
        }
        /*if (TextUtils.isEmpty(etLastName.getText())) {
            etLastName.setError(getString(R.string.enter_value));
            flag = false;
        }
        if (TextUtils.isEmpty(etPhoneNumber.getText())) {
            etPhoneNumber.setError(getString(R.string.enter_value));
            flag = false;
        }else {
            String phoneNumber = etPhoneNumber.getText().toString();
            if (phoneNumber.length() < 10) {
                etPhoneNumber.setError(getString(R.string.enter_valid_number));
                flag = false;
            }

        }

        /*if (TextUtils.isEmpty(etEmail.getText())) {
            etEmail.setError(getString(R.string.enter_value));
            flag = false;
        }else {
            String Email = etEmail.getText().toString();
            if (Email.)
            {
                etEmail.setError("enter valid Email Id");
                flag = false;
            }
        }*/

        return flag;
    }




}







