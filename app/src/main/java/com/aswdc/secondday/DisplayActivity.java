package com.aswdc.secondday;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class DisplayActivity extends AppCompatActivity {

    TextView value;
    TextView textView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        initViewReference();
        bindViewValues();
    }

    void initViewReference()
    {
        textView = findViewById(R.id.tvActDisplayName);
        value  = findViewById(R.id.tvDisplayActPhoneNumber);
    }

    void bindViewValues() {

        // get the text from MainActivity
        Intent intent = getIntent();
        String name = intent.getStringExtra("Name");
        String phoneNumber = intent.getStringExtra("PhoneNumber");

        // use the text in a TextView
        textView.setText(name + "\n " + phoneNumber);
        value.setText("   " + phoneNumber);
    }
}