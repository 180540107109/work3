package com.aswdc.secondday;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button btnForm;
    Button btnCalculator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewReference();
        initViewEvent();
    }


        void initViewReference() {
            btnForm = findViewById(R.id.btnActForm);
            btnCalculator = findViewById(R.id.btnActCalculator);
        }
    void initViewEvent() {
        btnForm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent formIntent = new Intent(MainActivity.this, formActivity.class);
                startActivity(formIntent);
            }
        });
        btnCalculator.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent calculatorIntent = new Intent(MainActivity.this, calculatorActivity.class);
                startActivity(calculatorIntent);
            }
        });

    }
}